module.exports = {
    devServer: {
        clientLogLevel: 'info',
        https: true,
    },
    publicPath: process.env.NODE_ENV === 'production'
        ? '/targame/'
        : '/'
    // compilerOptions: {
    //     isCustomElement: (tag) => tag.startsWith('a-')
    // }
};