# Targame

Targame est une application web permettant de jouer à un jeu de tir à la première personne en réalité augmenté.
Les tirs sont simulés par le scan de QR Codes mis en évidences sur les participants.

## Comment jouer?

Chaque joueur doit se munir de deux sacs à dos avec un qr code collé sur chaque.
Les qr codes doit être de valeur F pour l'avant, B pour l'arrière suivi du numéro du joueur.
Exemple: F3 représente le qrcode avant du joueur 3
Ensuite chacun doit se connecter en renseignant son numéro de QR Code et son nom.
On peut pour le moment définir le mode de jeu en tant qu'administrateur en se connectant avec le nom admin.

Deux modes de jeu sont proposés:

- le mode ffa: l'objectif est de faire le plus de kill possible.
- le mode infecté: la partie commence avec un infecté (lampe allumée) et il doit contaminer les autres. (très instable pour le moment)

## Technologies utilisées

- Vue: Frontend
- Vue Router: Pour les routage des pages
- Vue X: Pour le partage des données en pages et communication backend
- Vue-qrcode-reader: Scanneur de qrcode
- Firebase: Backend

## Fonctionnalités web utilisées

- Shape detection: pour détecter les qrcodes (dans le scanneur)
- Vibration: pour les tirs / morts
- Torch: pour identifié les infectés (dans le scanneur)

## Architecture

- src/
  - assets/ : Ressources médias
  - interfaces/ : Interfaces TS
  - mixins/ : Partages de propriété de component
  - store/ : Stockages des données
  - views/ : Pages

## Problèmes rencontrées

- Le projet ne fonctionne que sur Brave pour le moment:
  - Impossible de zoomer la caméra sur d'autres navigateur que Brave et Chrome
  - Surcharge de mémoire du scanneur dans les version 95+ de Chrome
- Le compteur de résurrection n'affiche pas de bonne valeurs et bloque le joueur
- La propagation des infectés est défaillante
- Le compteur de kill est bloqué à 0

## Installation

```
npm install
```

### Déboggage local

```
npm run serve
```

### Compilation

```
npm run build
```

### Correction syntaxique

```
npm run lint
```
