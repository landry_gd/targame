import { initializeApp } from "firebase/app";
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import { key, store } from "./store";

const firebaseConfig = {
  apiKey: "AIzaSyDEak9qLS5NFptZM90YpBepputpmdVdTZU",
  authDomain: "enjmin-ar-fps.firebaseapp.com",
  databaseURL:
    "https://enjmin-ar-fps-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "enjmin-ar-fps",
  storageBucket: "enjmin-ar-fps.appspot.com",
  messagingSenderId: "870229273989",
  appId: "1:870229273989:web:777888b621363cd4f628ec",
};

// Initialize Firebase
initializeApp(firebaseConfig);

createApp(App).use(router).use(store, key).mount("#app");
