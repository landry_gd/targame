import { getAuth, signInAnonymously } from "@firebase/auth";
import { child, get, getDatabase, ref, set, update } from "firebase/database";
import { InjectionKey } from "vue";
import { createStore, Store } from "vuex";
import User from "../interfaces/User";

// define your typings for the store state
export interface State {
  user: User;
}

export const key: InjectionKey<Store<State>> = Symbol();

export const store = createStore<State>({
  state: {
    user: {
      id: "",
      name: "",
      qrcode: 0,
      kill: 0,
      death: 0,
    },
  },
  getters: {
    // setUser (state, payload) {
    //     state.user = payload
    // }
  },
  mutations: {
    setUser(state, payload) {
      state.user = payload;
      console.log("user state changed:", state.user);
    },
  },
  actions: {
    /**
     * Supprime toutes les données de la base
     * @param context Contexte de l'action
     */
    async deleteAll(context) {
      const db = getDatabase();
      await set(ref(db, "/"), null);
      console.log("Deleted All!");
    },
    /**
     * Active le mode infecté
     * @param context Contexte de l'action
     */
    async infectedMode(context) {
      const db = getDatabase();
      await update(ref(db, "settings"), { mode: "infected" });
      await this.dispatch("reinitUsers");
      const infectedUser = await this.dispatch("getRandomUser");
      infectedUser.infected = true;
      await this.dispatch("updateUser", infectedUser);
      console.log("Infected mode!");
    },
    /**
     * Active le mode FFA
     * @param context Contexte de l'action
     */
    async ffaMode(context) {
      const db = getDatabase();
      await update(ref(db, "settings"), { mode: "ffa" });
      console.log("FFA mode!");
    },
    /**
     * Réinitialise les utilisateurs
     * @param context Contexte de l'action
     */
    async reinitUsers(context) {
      const users: User[] = await this.dispatch("getUsers");
      console.log("users", users);
      for (let user of users) {
        user = {
          id: user.id,
          name: user.name,
          qrcode: user.qrcode,
          kill: 0,
          death: 0,
        };
        await this.dispatch("updateUser", { user });
      }
    },
    /**
     * S'authentifie anonymement à Firebase
     * @param context Contexte de l'action
     * @param user Données entrées de l'utilisateur
     */
    async login(context, user) {
      const auth = getAuth();
      const credential = await signInAnonymously(auth);
      console.log("Signed in!");
      this.state.user.id = credential.user.uid;
      const existingUser = await this.dispatch("getUser", {
        id: this.state.user.id,
      });
      // if (existingUser)
      //   await this.dispatch("deleteUser", { id: this.state.user.id });
      await this.dispatch("deleteUsersByQrCode", { qrcode: user.qrcode });
      await this.dispatch("setUser", user);
    },
    /**
     * Retourne un utilisateur par id
     * @param context Contexte de l'action
     * @param id Identifiant de l'utilisateur cherché
     */
    async getUser(context, { id }: { id: string }): Promise<User | undefined> {
      if (id) {
        const dbRef = ref(getDatabase());
        const snapshot = await get(child(dbRef, `users/${id}`));
        if (snapshot.exists()) {
          return snapshot.val();
        }
        return undefined;
      }
    },
    /**
     * Retourne tout les utilisateurs
     * @param context Contexte de l'action
     */
    async getUsers(context): Promise<User[] | undefined> {
      const dbRef = ref(getDatabase());
      const snapshot = await get(child(dbRef, `users`));
      if (snapshot.exists()) {
        return Object.values(snapshot.val());
      }
      return [];
    },
    /**
     * Retourne un utilisateur aléatoire
     * @param context Contexte de l'action
     */
    async getRandomUser(context): Promise<User | undefined> {
      const users = await this.dispatch("getUsers");
      const randomIndex = Math.floor(users.length * Math.random());
      return users[randomIndex];
    },
    /**
     * Retourne un utilisateur par numéro de qrcode
     * @param context Contexte de l'action
     * @param qrCode Numéro de qrcode de l'utilisateur
     */
    async getUserByQrCode(
      context,
      { qrCode }: { qrCode: number }
    ): Promise<User | undefined> {
      if (qrCode) {
        const dbRef = ref(getDatabase());
        const snapshot = await get(child(dbRef, `users/`));
        if (snapshot.exists()) {
          const users: User[] = Object.values(snapshot.val());
          const targetUser = users.filter((user) => user.qrcode == qrCode)[0];
          return targetUser;
        }
        return undefined;
      }
    },
    /**
     * Supprime les utilisateurs associés à un numéro de qrcode
     * @param context Contexte de l'action
     * @param qrCode Numéro de qrcode de ou des utilisateurs
     */
    async deleteUsersByQrCode(
      context,
      { qrcode }: { qrcode: number }
    ): Promise<User | undefined> {
      console.log(qrcode);
      if (qrcode !== undefined) {
        console.log("deleteUsersByQrCode", qrcode);
        const dbRef = ref(getDatabase());
        const snapshot = await get(child(dbRef, `users/`));
        if (snapshot.exists()) {
          const users: User[] = Object.values(snapshot.val());
          console.log("users", users);
          for (const user of users) {
            console.log({ target: user.qrcode, source: qrcode });
            if (user.qrcode === qrcode) {
              await this.dispatch("deleteUser", { id: user.id });
            }
          }
        }
        return undefined;
      }
    },
    /**
     * Pose une valeur pour un utilisateur
     * @param context Contexte de l'action
     * @param user Utilisateur à définir
     */
    async setUser(context, user: User) {
      if (user) {
        const db = getDatabase();
        await set(ref(db, "users/" + user.id), user);
        console.log("Created user!");
      }
    },
    /**
     * Met à jour un utilisateur
     * @param context Contexte de l'action
     * @param user Utilisateur à mettre à jour
     */
    async updateUser(context, user: User) {
      if (user) {
        const db = getDatabase();
        await update(ref(db, "users/" + user.id), user);
        console.log("Created user!");
      }
    },
    /**
     * Supprime un utilisateur par id
     * @param context Contexte de l'action
     * @param id Identifiant de l'utilisateur à supprimer
     */
    async deleteUser(context, { id }: { id: string }) {
      if (id) {
        const db = getDatabase();
        await set(ref(db, "users/" + id), null);
        console.log("Deleted existing user!");
      }
    },
  },
});
