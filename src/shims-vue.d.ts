/* eslint-disable */
declare module "*.vue" {
  import type { DefineComponent } from "vue";
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

declare module "vue-qrcode-reader" {
  declare class QrcodeStream {}
  export class QrcodeStream {}
}
