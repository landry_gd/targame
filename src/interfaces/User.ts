// Classe de l'utilisateur
export default interface User {
  // Identifiant unique
  id: string;
  // Nom d'utilisateur
  name: string;
  // Numéro de Qrcode associé
  qrcode: number;
  // Nombre de kills
  kill: number;
  // Nombre de morts
  death: number;
  // Timestamp de la dernière mort
  deathTime?: number;
  // Vrai si infecté
  infected?: boolean;
  // Vrai si administrateur
  admin?: boolean;
}
